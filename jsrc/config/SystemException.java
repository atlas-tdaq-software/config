package config;

/**
 * Thrown to indicate that there is a system exception
 */

@SuppressWarnings("serial")
public class SystemException extends ConfigException
  {
    public SystemException(String message)
      {
        super(message);
      }

    public SystemException(String message, Exception cause)
      {
        super(message, cause);
      }

    public SystemException(Exception cause)
      {
        super(cause);
      }
    
    public SystemException(String id, ers2idl.Issue cause)
      {
        super("remote method failed with " + id + " exception", ers2idl.Converter.idl2ersIssue(cause));
      }
  }